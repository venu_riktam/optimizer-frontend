module.exports = {
    parserOptions: {
        parser: 'babel-eslint'
    },
    extends: [
        'plugin:vue/recommended',
        'standard'
    ],
    plugins: [
        'vue'
    ],
    rules: {
        "vue/max-attributes-per-line": [
          0,
          {
            singleline: 10,
            multiline: {
              max: 10,
              allowFirstLine: true
            }
          }
        ],
    }
}